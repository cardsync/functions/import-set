package sync

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"cloud.google.com/go/pubsub"

	kitlog "github.com/go-kit/kit/log"
	"github.com/pkg/errors"

	pq "github.com/lib/pq"
)

type PubSubMessage struct {
	Data []byte `json:"data"`
}

var (
	psql   *sql.DB
	logger kitlog.Logger

	topic *pubsub.Topic
)

func init() {
	var err error

	{
		logger = kitlog.NewJSONLogger(os.Stdout)
		logger = kitlog.With(logger, "timestamp", kitlog.DefaultTimestampUTC, "transport", "pubsub")
	}

	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not connect to database", "err", err)
			os.Exit(1)
		}
		logger.Log("level", "debug", "msg", "connected to database", "url", os.Getenv("DATABASE_URL"))
	}

	// Only allow 1 connection to the database to avoid overloading it.
	psql.SetMaxIdleConns(1)
	psql.SetMaxOpenConns(1)

	{
		client, err := pubsub.NewClient(context.Background(), "synkt-1556838469966")
		if err != nil {
			logger.Log("level", "fatal", "msg", "error creating pubsub client", "err", err)
			os.Exit(1)
		}

		topic = client.Topic("sync-set-prices")
	}
}

type scryfallSet struct {
	Name       string `json:"name"`
	Code       string `json:"code"`
	SetType    string `json:"set_type"`
	ReleasedAt string `json:"released_at"`
}

type scryfallSearch struct {
	HasMore  bool           `json:"has_more"`
	NextPage string         `json:"next_page"`
	Data     []scryfallCard `json:"data"`
}

type scryfallCard struct {
	ID            string   `json:"id"`
	Name          string   `json:"name"`
	TypeLine      string   `json:"type_line"`
	Rarity        string   `json:"rarity"`
	MultiverseIDs []int    `json:"multiverse_ids"`
	Number        *string  `json:"collector_number"`
	ManaCost      *string  `json:"mana_cost"`
	CMC           *float64 `json:"cmc"`
	Colors        []string `json:"colors"`
	ColorIdentity []string `json:"color_identity"`
	Text          string   `json:"text"`
	Flavor        string   `json:"flavor_text"`
	Artist        string   `json:"artist"`
	Power         *string  `json:"power"`
	Toughness     *string  `json:"toughness"`
	Loyalty       *string  `json:"loyalty"`
	Layout        string   `json:"layout"`
	Reserved      bool     `json:"reserved"`
	Border        string   `json:"border_color"`
	Images        struct {
		Large string `json:"large"`
	} `json:"image_uris"`

	Foil    bool `json:"foil"`
	NonFoil bool `json:"nonfoil"`

	Promo        bool     `json:"promo"`
	FrameEffects []string `json:"frame_effects"`
}

func (c scryfallCard) MultiverseID() *int {
	if len(c.MultiverseIDs) > 0 {
		return &c.MultiverseIDs[0]
	}

	return nil
}

func (c scryfallCard) FormattedName() string {
	for _, fe := range c.FrameEffects {
		if fe == "extendedart" {
			return fmt.Sprintf("%s (%s)", c.Name, "Extended Art")
		} else if fe == "showcase" {
			return fmt.Sprintf("%s (%s)", c.Name, "Showcase")
		}
	}

	if c.Border == "borderless" {
		return fmt.Sprintf("%s (%s)", c.Name, "Borderless")
	}

	if strings.HasPrefix(c.Name, "Basic Land") && c.Number != nil {
		return fmt.Sprintf("%s (%s)", c.Name, *c.Number)
	}

	return c.Name
}

func ImportSet(ctx context.Context, m PubSubMessage) error {
	var (
		url   string
		setID string
		set   scryfallSet

		resp *http.Response
		err  error
	)
	logger.Log("level", "debug", "msg", "received message", "data", string(m.Data))

	resp, err = http.Get(fmt.Sprintf("https://api.scryfall.com/sets/%s", string(m.Data)))
	if err != nil {
		logger.Log("level", "error", "msg", "error executing http request", "err", err)
		return errors.Wrap(err, "error getting scryfall sets")
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&set)
	if err != nil {
		logger.Log("level", "error", "msg", "error decoding json", "err", err)
		return errors.Wrap(err, "error decoding json")
	}

	// look for the set
	err = psql.QueryRow(`SELECT id FROM card_sets WHERE code = $1`, m.Data).Scan(&setID)
	// if it doesn't exist, create it
	if err == sql.ErrNoRows {
		_, err = psql.Exec(
			`INSERT INTO card_sets (
				name, code, set_type, released_at, created_at, updated_at
			) VALUES (
				$1, $2, $3, $4, now(), now()
			)`,
			set.Name, set.Code, set.SetType, set.ReleasedAt,
		)

		if err != nil {
			return errors.Wrap(err, "error creating card set")
		}

		_ = psql.QueryRow(`SELECT id FROM card_sets WHERE code = $1`, m.Data).Scan(&setID)
	} else if err != nil {
		return errors.Wrap(err, "error selecting card set id")
	}

	url = fmt.Sprintf("https://api.scryfall.com/cards/search?format=json&include_extras=false&include_multilingual=false&order=set&page=1&q=e%%3A%s&unique=prints", string(m.Data))

	for {
		resp, err = http.Get(url)
		if err != nil {
			logger.Log("level", "error", "msg", "error executing http request", "err", err)
			return err
		}
		defer resp.Body.Close()

		var result scryfallSearch

		err = json.NewDecoder(resp.Body).Decode(&result)
		if err != nil {
			logger.Log("level", "error", "msg", "error decoding json", "err", err)
			return err
		}

		if len(result.Data) == 0 {
			return fmt.Errorf("No results found in scryfall for %s", m.Data)
		}

		for _, c := range result.Data {
			var cardID string

			// look for the card
			err = psql.QueryRow(`SELECT id FROM cards WHERE scryfall_id = $1`, c.ID).Scan(&cardID)
			// if it doesn't exist, create it
			if err == sql.ErrNoRows {
				_, err = psql.Exec(
					`INSERT INTO cards (
						card_set_id, scryfall_id, name, type_line, rarity, multiverseid,
						number, names, mana_cost, cmc, colors, color_identity, supertypes,
						types, subtypes, text, flavor, artist, power, toughness, loyalty,
						variations, watermark, border, reserved, layout,
						created_at, updated_at,
						photo, image
					) VALUES (
						$1, $2, $3, $4, $5, $6,
						$7, $8, $9, $10, $11, $12, $13,
						$14, $15, $16, $17, $18, $19, $20, $21,
						$22, $23, $24, $25, $26,
						now(), now(),
						$27, $28
					)`,
					setID, c.ID, c.Name, c.TypeLine, c.Rarity, c.MultiverseID(),
					c.Number, nil, c.ManaCost, c.CMC, pq.Array(c.Colors), pq.Array(c.ColorIdentity), nil,
					nil, nil, c.Text, c.Flavor, c.Artist, c.Power, c.Toughness, c.Loyalty,
					nil, nil, c.Border, c.Reserved, c.Layout,
					c.Images.Large, nil,
				)

				if err != nil {
					logger.Log("level", "error", "msg", "error creating card", "err", err, "name", c.Name)
					continue
				}

				err = psql.QueryRow(`SELECT id FROM cards WHERE scryfall_id = $1`, c.ID).Scan(&cardID)
			} else if err != nil {
				return errors.Wrap(err, "error selecting card")
			}

			if c.NonFoil {
				_, err = psql.Exec(
					`INSERT INTO card_conditions (
						card_id, condition, created_at, updated_at
					) VALUES (
						$1, 'near-mint', now(), now()
					), (
						$1, 'slightly-played', now(), now()
					), (
						$1, 'heavily-played', now(), now()
					)`, cardID,
				)
				if err != nil {
					return errors.Wrap(err, "error creating non foil card conditions")
				}
			}

			if c.Foil {
				_, err = psql.Exec(
					`INSERT INTO card_conditions (
						card_id, condition, created_at, updated_at
					) VALUES (
						$1, 'foil-near-mint', now(), now()
					), (
						$1, 'foil-slightly-played', now(), now()
					), (
						$1, 'foil-heavily-played', now(), now()
					)`, cardID,
				)
				if err != nil {
					return errors.Wrap(err, "errors creating foil card conditions")
				}
			}
		}

		if !result.HasMore {
			break
		}

		url = result.NextPage
	}

	_, err = psql.Exec(
		`INSERT INTO user_card_conditions (
			user_id, card_condition_id, created_at, updated_at
		) SELECT users.id, card_conditions.id, now(), now()
			FROM users, card_conditions
			JOIN cards ON card_conditions.card_id = cards.id
			JOIN card_sets ON cards.card_set_id = card_sets.id
			WHERE card_sets.id = $1
		ON CONFLICT DO NOTHING`,
		setID,
	)
	if err != nil {
		return errors.Wrap(err, "error creating user card conditions")
	}

	return err
}
