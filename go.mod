module gitlab.com/cardsync/functions/import-set

go 1.12

require (
	cloud.google.com/go v0.44.1
	cloud.google.com/go/datastore v1.0.0
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/lib/pq v1.4.0
	github.com/pkg/errors v0.9.1
)
