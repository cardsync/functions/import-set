# Functions - Import Set

This is a Google Cloud Function to import a set.

It can also be ran locally with

`DATABASE_URL=<pg-url> go run ./cmd/import-set/main.go`
