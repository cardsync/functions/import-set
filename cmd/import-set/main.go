package main

import (
	"context"

	importset "gitlab.com/cardsync/functions/import-set"
)

func main() {
	for _, set := range []string{"ss3"} {
		err := importset.ImportSet(context.Background(), importset.PubSubMessage{Data: []byte(set)})
		if err != nil {
			println(err.Error())
		}
	}
}
